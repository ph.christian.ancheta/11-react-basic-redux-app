# Basic Redux App - React App (Platform Electives Objective)
A simple React App that allows users to use redux in a simple increment/decrement counter process. The user could also toggle show of the incremental counter using redux. Lastly, a simple authentication process is also displayed wherein other options in the navigation bar are hidden if the user has not yet logged in. The main objective that was showcased in creating the app was the ability to store sitewide states through Redux and Redux Tools.

## Setup

```
npm install
npm run start
```
